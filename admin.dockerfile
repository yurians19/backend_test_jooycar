FROM node:14.17.0

WORKDIR /app

COPY ./package*.json ./

RUN npm install

COPY . /app/

RUN npm run build

EXPOSE 3000
CMD [ "node", "dist/main.js" ]
