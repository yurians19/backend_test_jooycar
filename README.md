# API ADMINS JOOYCAR 
- Git pull to repository
## Steps to run the project:

1. Create file .env with variable:

PORT=3002
MONGO_CONNECTION_URI=mongodb://mongodb/admins
SERVER_SWAGGER=http://localhost:3002
WHITE_LIST=http://localhost:3002
BASE_URL_NOTIFICATIONS=https://virtserver.swaggerhub.com/CONTABILIDAD/JooycarTest/1.0.0

2. Move to backend_test_jooycar folder
3. Run `docker-compose build`
4. Run `docker-compose up -d`


## Swagger:

Navigate to `http://localhost:3002/api/docs`

