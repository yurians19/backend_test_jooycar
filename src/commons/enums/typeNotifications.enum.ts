export enum ETypeNotifications {
  send = 'send',
  confirm = 'confirm',
  reject = 'reject',
}
