import { HttpStatus } from '@nestjs/common'

export const httpExceptionNotFound = {
  statusCode: HttpStatus.UNPROCESSABLE_ENTITY,
  errorCode: HttpStatus.UNPROCESSABLE_ENTITY,
  srcMessage: 'Resource not found',
  translatedMessage: 'Recurso no encontrado',
}
