import { HttpService } from '@nestjs/axios'
import { HttpException, HttpStatus, Injectable } from '@nestjs/common'
import { catchError, map } from 'rxjs'
import { IBodyNotifications } from 'src/commons/interfaces'

@Injectable()
export class NotificationsService {

  constructor(private readonly httpService: HttpService) {}

  async send(body: IBodyNotifications): Promise<any> {
    try {
      return await this.httpService
        .post(`${process.env.BASE_URL_NOTIFICATIONS}/api/notifications/v1/actions`, body)
        .pipe(map((response) =>response.data))
        .toPromise()
    } catch (error) {
      return error
    }
  }
}
