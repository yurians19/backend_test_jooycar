import { PipeTransform, Injectable, ArgumentMetadata, HttpException, HttpStatus } from '@nestjs/common';
import { ObjectSchema } from 'joi';
import { httpExceptionInvalidAttribute } from '..';

@Injectable()
export class JoiValidationPipe implements PipeTransform {
  constructor(private schema: ObjectSchema) {}

  transform(value: any, metadata: ArgumentMetadata) {
    const { error } = this.schema.validate(value);
    if (error) {
      throw new HttpException(httpExceptionInvalidAttribute, HttpStatus.UNPROCESSABLE_ENTITY)
    }
    return value;
  }
}