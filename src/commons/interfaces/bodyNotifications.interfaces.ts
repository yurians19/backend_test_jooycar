import { ETypeNotifications } from "..";

export interface IBodyNotifications {
    type: ETypeNotifications
    id: string
  }