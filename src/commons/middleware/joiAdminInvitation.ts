import * as Joi from 'joi';
import { EEstatusAdmin } from 'src/admin/enum';

export const joiSchemaAdminInvitation = Joi.object({
  name: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string().email().required(),
  role: Joi.string().required(),
})

export const joiSchemaAdminUpdate = Joi.object({
  name: Joi.string().optional(),
  lastName: Joi.string().optional(),
  email: Joi.string().email().optional(),
  role: Joi.string().optional(),
  status: Joi.string().optional().valid(EEstatusAdmin.active, EEstatusAdmin.disable),
})
