import * as Joi from 'joi'

export const joiSchemaClause = Joi.object({
  MONGO_CONNECTION_URI: Joi.string().required(),
  WHITE_LIST: Joi.string().required(),
  BASE_URL_NOTIFICATIONS: Joi.string().required(),
})
