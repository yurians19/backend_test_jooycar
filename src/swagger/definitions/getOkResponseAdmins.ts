import { ApiResponseOptions } from '@nestjs/swagger'
import { AdminDto } from 'src/admin/dto'

export const getOkResponseAdmins: ApiResponseOptions = {
  description: 'Success',
  type: AdminDto,
  isArray: true,
}
