import { ApiBodyOptions } from '@nestjs/swagger'
import { AdminUpdate } from 'src/admin/dto'

export const apiBodyAdminsUpdate: ApiBodyOptions = {
  description: 'The requested Body',
  type: AdminUpdate,
}
