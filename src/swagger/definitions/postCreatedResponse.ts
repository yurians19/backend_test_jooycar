import { ApiBodyOptions } from '@nestjs/swagger'
import { AdminDto } from 'src/admin/dto'

export const postCreatedResponse: ApiBodyOptions = {
  description: 'Success',
  type: AdminDto
}
