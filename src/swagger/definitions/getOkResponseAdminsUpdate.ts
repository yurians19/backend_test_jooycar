import { ApiResponseOptions } from '@nestjs/swagger'
import { AdminDto } from 'src/admin/dto'

export const getOkResponseAdminsUpdate: ApiResponseOptions = {
  description: 'Success',
  type: AdminDto,
}
