import { ApiParamOptions } from '@nestjs/swagger'

export const apiParamsAdmins: ApiParamOptions = { 
  name: 'id',
  type: 'string',
  description: 'Admin Id',
  example: 'f5bb33ff-fd72-4c98-a811-17b5dce46be9'
 }
