import { ApiBodyOptions } from '@nestjs/swagger'
import { AdminInvitation } from 'src/admin/dto'

export const apiBodyAdminsUpdatePost: ApiBodyOptions = {
  description: 'The requested Body',
  type: AdminInvitation,
}
