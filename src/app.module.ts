import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { joiSchemaClause, NotificationsService } from './commons'
import { DatabaseModule } from './database/mongo/database.module'
import { AdminModule } from './admin/admin.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: joiSchemaClause
    }),
    AdminModule,
    HttpModule,
    DatabaseModule,
  ],
  controllers: [AppController],
  providers: [AppService, NotificationsService],
  exports: [NotificationsService]
})
export class AppModule {}
