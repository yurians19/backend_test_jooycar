import { ValidationPipe, VersioningType } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import swagger from './swagger'
import morgan from 'morgan'
import helmet from 'helmet'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.use(morgan('tiny'))
  const configService = app.get(ConfigService)
  app.enableCors({
    origin: configService.get('WHITE_LIST').split(';'),
    allowedHeaders: 'Origin, X-Requested-With, Content-Type, Range, Content-Range, Accept, Authorization, Accept-Language, cache-control',
    exposedHeaders: 'Content-Type, Range, Method'
  })
  app.setGlobalPrefix('api')
  app.useGlobalPipes(new ValidationPipe())
  app.enableVersioning({
    type: VersioningType.URI
  })
  swagger(app, configService)
  app.use(
    helmet({
      contentSecurityPolicy: {
        directives: {
          defaultSrc: [`'self'`],
          styleSrc: [`'self'`, `'unsafe-inline'`],
          imgSrc: [`'self'`, 'data:', 'validator.swagger.io'],
          scriptSrc: [`'self'`, `https: 'unsafe-inline'`]
        }
      }
    })
  )
  await app.listen(configService.get('PORT') || 3002)
}
bootstrap()
