import { Injectable, HttpStatus, HttpException } from '@nestjs/common'
import { AdminDocument } from './schemas/admin.schema';
import { AdminRepository } from './admin.repository'
import { FilterQuery } from 'mongoose'
import { AdminInvitation, AdminUpdate } from './dto'
import { EEstatusAdmin } from './enum'
import { ETypeNotifications, httpExceptionDuplicated, httpExceptionNotAlowed, NotificationsService } from 'src/commons'
import { IBodyNotifications } from 'src/commons/interfaces'
import { httpExceptionNotFound } from 'src/commons/types/badRequest/httpExceptionNotFound'

@Injectable()
export class AdminService {
  constructor(
    private readonly adminRepository: AdminRepository,
    private readonly notificationsService: NotificationsService,
  ) {}

  async getAdmin(entityFilterQuery: FilterQuery<AdminDocument>): Promise<AdminDocument[]> {
    return this.adminRepository.find(entityFilterQuery)
  }

  async getAdminOne(entityFilterQuery: FilterQuery<AdminDocument>): Promise<AdminDocument> {
    return this.adminRepository.findOne(entityFilterQuery)
  }

  async createAdmin(admin: AdminInvitation): Promise<AdminDocument> {
    const { email } :AdminInvitation = admin
    let newAdmin: AdminDocument

    const oldAdmin: AdminDocument = await this.getAdminOne({ email })
    
    if (!oldAdmin) {
      newAdmin = await this.adminRepository.create(admin)
    }
    if (oldAdmin?.status === EEstatusAdmin.deleted) {
      newAdmin = await this.adminRepository.findOneAndUpdate({ email }, { ...admin, status: EEstatusAdmin.pending} )
    }
    if (oldAdmin && oldAdmin?.status !== EEstatusAdmin.deleted) {
      throw new HttpException(httpExceptionDuplicated, HttpStatus.CONFLICT)
    }
    await this.sendNotification({ type: ETypeNotifications.confirm, id: newAdmin.id})
    return newAdmin
  }

  async updateAdmin(
    idParam: string,
    adminUpdate: AdminUpdate
  ): Promise<AdminDocument> {

    const oldAdmin: AdminDocument = await this.getAdminOne({ id: idParam })
    this.AdminExistsValidate(oldAdmin)
    const { email, status, id }: AdminDocument = oldAdmin

    if (email !== adminUpdate.email && status !== EEstatusAdmin.pending) {
      throw new HttpException(httpExceptionNotAlowed, HttpStatus.FORBIDDEN)
    }

    if (status === EEstatusAdmin.pending && adminUpdate.status === EEstatusAdmin.active) {
      await this.sendNotification({ type: ETypeNotifications.confirm, id})
    }

    if (status === EEstatusAdmin.pending && adminUpdate.status === EEstatusAdmin.disable) {
      await this.sendNotification({ type: ETypeNotifications.reject, id})
    }
    return this.adminRepository.findOneAndUpdate({ id }, adminUpdate)
  }

  async deleteAdmin(id: string): Promise<AdminDocument> {
    const oldAdmin: AdminDocument = await this.getAdminOne({ id })
    this.AdminExistsValidate(oldAdmin)
    return this.adminRepository.findOneAndUpdate({ id }, { status: EEstatusAdmin.deleted })
  }

  async sendNotification(bodyNotifications: IBodyNotifications): Promise<void> {
    await this.notificationsService.send(bodyNotifications)
  }

  AdminExistsValidate(admin: AdminDocument): void {
    if ((admin && admin?.status === EEstatusAdmin.deleted) || !admin) {
      throw new HttpException(httpExceptionNotFound, HttpStatus.UNPROCESSABLE_ENTITY)
    }
  }
}
