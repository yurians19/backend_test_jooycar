import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { Admin, AdminSchema } from './schemas/admin.schema'
import { AdminController } from './admin.controller'
import { AdminRepository } from './admin.repository'
import { AdminService } from './admin.service'
import { NotificationsService } from 'src/commons'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Admin.name, schema: AdminSchema }]),
    HttpModule
  ],
  controllers: [AdminController],
  providers: [AdminService, AdminRepository, NotificationsService]
})
export class AdminModule {}
