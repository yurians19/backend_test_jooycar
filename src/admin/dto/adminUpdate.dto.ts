import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsEmail } from 'class-validator';
import { EEstatusAdmin } from 'src/admin/enum';

export class AdminUpdate {
  @ApiProperty({ example: 'Admin', nullable: true })
  name: string

  @ApiProperty({ example: '1', nullable: true  })
  lastName: string

  @ApiProperty({ example: 'admin1@fleetr.com', format: 'email', nullable: true  })
  @IsEmail()
  email: string

  @ApiProperty({ example: 'admin.fleetro', nullable: true  })
  role: string

  @ApiProperty({ example: EEstatusAdmin.active, enum: [ EEstatusAdmin.active, EEstatusAdmin.disable ] })
  status: EEstatusAdmin
}
