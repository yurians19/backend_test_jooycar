export * from './statusAdmin.enum'

export const getSupplyLimits = [15, 20, 50, 100]
export const getSupplyLimitsOffLine = [500, 1000, 2500, 5000]
