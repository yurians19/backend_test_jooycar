export enum EEstatusAdmin {
  pending = 'pending',
  active = 'active',
  disable = 'disable',
  deleted = 'deleted'
}
