import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { EntityRepository } from '../database/mongo/entity.repository'
import { Admin, AdminDocument } from './schemas'

@Injectable()
export class AdminRepository extends EntityRepository<AdminDocument> {
  constructor(@InjectModel(Admin.name) adminModel: Model<AdminDocument>) {
    super(adminModel)
  }
}
