import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common'
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiResponse,
  ApiBody,
  ApiParam
} from '@nestjs/swagger'
import {
  AdminInvitation, AdminUpdate,
} from './dto'
import { Admin, AdminDocument } from './schemas/admin.schema'
import { AdminService } from './admin.service'
import { 
  apiBodyAdminsUpdate, 
  apiNotFoundResponse, 
  apiParamsAdmins, apiResponseInvalid, 
  deleteOperationAdmins, 
  getOkResponseAdmins, 
  getOkResponseAdminsUpdate, 
  getOperationAdmins, 
  getResponseInvalid, 
  postCreatedResponse,
  apiBodyAdminsUpdatePost, 
  postOperationAdmins, 
  apiResponseNotAllowed} from '../swagger/definitions'
import { EEstatusAdmin } from './enum'
import { JoiValidationPipe } from '../commons/pipes/validation.pipe';
import { joiSchemaAdminInvitation, joiSchemaAdminUpdate } from 'src/commons'

@ApiBearerAuth()
@ApiTags('Admins')
@ApiExtraModels(Admin)
@Controller({
  version: '1',
  path: 'admins'
})
export class AdminController {
  constructor(private readonly adminService: AdminService) {}

  @Get()
  @ApiOkResponse(getOkResponseAdmins)
  @ApiOperation(getOperationAdmins)
  @ApiNotFoundResponse(apiNotFoundResponse)
  @ApiResponse(getResponseInvalid)
  async getAdmin(): Promise<Admin[]> {
    return this.adminService.getAdmin({ status: { $in: [ EEstatusAdmin.active, EEstatusAdmin.pending ] } })
  }

  @ApiOperation(postOperationAdmins)
  @ApiBody(apiBodyAdminsUpdatePost)
  @ApiCreatedResponse(postCreatedResponse)
  @ApiResponse(getResponseInvalid)
  @ApiResponse(apiResponseInvalid)
  @ApiNotFoundResponse(apiNotFoundResponse)
  @Post()
  async createAdmin(
    @Body(new JoiValidationPipe(joiSchemaAdminInvitation)) admin: AdminInvitation
  ): Promise<Admin> {
    return this.adminService.createAdmin(admin)
  }

  @ApiOperation(getOperationAdmins)
  @ApiParam(apiParamsAdmins)
  @ApiBody(apiBodyAdminsUpdate)
  @ApiOkResponse(getOkResponseAdminsUpdate)
  @ApiResponse(getResponseInvalid)
  @ApiResponse(apiResponseNotAllowed)
  @ApiNotFoundResponse(apiNotFoundResponse)
  @Patch(':id')
  async updateAdmin(
    @Param('id') id: string,
    @Body(new JoiValidationPipe(joiSchemaAdminUpdate)) updateAdmin: AdminUpdate
  ): Promise<AdminDocument> {
    return this.adminService.updateAdmin(id, updateAdmin)
  }

  @ApiOperation(deleteOperationAdmins)
  @ApiParam(apiParamsAdmins)
  @ApiOkResponse(getOkResponseAdminsUpdate)
  @ApiResponse(getResponseInvalid)
  @ApiNotFoundResponse(apiNotFoundResponse)
  @Delete(':id')
  async deleteAdmin(
    @Param('id') id: keyof Admin,
  ): Promise<Admin> {
    return this.adminService.deleteAdmin(id)
  }
}
