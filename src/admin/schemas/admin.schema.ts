import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document, Schema as mongooseSchema } from 'mongoose'
import { ApiProperty } from '@nestjs/swagger'
import { v4 as uuidv4 } from 'uuid'
import { EEstatusAdmin } from '../enum'

@Schema()
export class Admin {
  @Prop({ default: uuidv4() })
  @ApiProperty({ format: 'uuid' })
  id: string

  @Prop({ required: true })
  @ApiProperty()
  name: string

  @Prop({ required: true })
  @ApiProperty()
  lastName: string

  @Prop({ unique: true, required: true, format: 'email' })
  @ApiProperty()
  email: string

  @Prop({ required: true, default: EEstatusAdmin.pending})
  @ApiProperty()
  status: EEstatusAdmin

  @Prop({ required: true })
  @ApiProperty()
  role: string

  @ApiProperty({ nullable: true })
  @Prop({ required: false })
  picture: string
}

export type AdminDocument = Admin & Document
export const AdminSchema = SchemaFactory.createForClass(Admin)
